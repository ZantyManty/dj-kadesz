class Loader {
    
	constructor(fs){
		this._fs = fs;
		this._modules = [];
		this._reflectionData = [];
		this._cache = [];
	}
	
	loadServices(servicesFile){
		if (this._fs.existsSync(servicesFile)) {
			let serviceList = JSON.parse(this._fs.readFileSync(servicesFile, 'utf-8'));
			
			for(let k in serviceList){
				this._modules[k] = serviceList[k];
			}
			
		} else {
			console.log('[Services file] does not exists!');
		}
	}
	
	collectConstructorData(paths){
		var obj = this;
		return new Promise(function(resolve,reject){
			
			let isOk = true;
			for(let pathKey in paths){
				isOk = obj.getConstructorData(paths[pathKey]);
			}
			obj._cache['Loader'] = obj;
			
			if(isOk) resolve(); else reject('Loader path error!');
			
		});
	}
	
	getConstructorData(path){
		if(this._fs.existsSync(path)){
			let pattern = /class\s*([\S\s]+?)\s*{[\s]*?constructor\(\s*([\w\s,-]+)*\s*\)/i;
					
			let fileArray = this._fs.readdirSync(path);
			
			for(let key in fileArray){
				
				/*
				* Ha mappa
				*/
				if(this._fs.lstatSync(path + '/' + fileArray[key]).isDirectory()){
					this.getConstructorData(path + '/' + fileArray[key]);
				} else {
					let fileContent = fs.readFileSync(path + '/' + fileArray[key], 'utf-8');
					let match = fileContent.match(pattern);
					
					if(match != null){
						let className = match[1];
						/*
						* Ha vannak konstruktor paraméterek
						*/						
						let constructorArgs = (match[2] != undefined) ? match[2].toString().split(',') : [];
						
						/*
						* Eltároljuk az adatokat
						*/
						this._reflectionData[className] = [];
						this._reflectionData[className]['Dependencies'] = constructorArgs;
						this._reflectionData[className]['Path'] = path + '/' + fileArray[key];
					}
				}
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	resolve(className){
		let requirePath = '.' + this._reflectionData[className]['Path'];

		let obj = require(requirePath);
		
		let dependencyList = (this._reflectionData[className]['Dependencies'] != undefined) ? this._reflectionData[className]['Dependencies']: [];
		
		var dependencies = [];
		//Kell egy dummy első elemnek,mert azt eldobja valamiért
		dependencies[0] = '';
		
		if(dependencyList.length > 0){
			
			for(let key in dependencyList){
				let service = dependencyList[key];
				let serviceInstance = null;
				
				if(this._cache[service] == undefined){
					
					if(this._modules[service] !== undefined){
						serviceInstance = require(this._modules[service]);
					} else {
						serviceInstance = this.resolve(service);
					}
					
					this._cache[service] = serviceInstance;
				} else {
					serviceInstance = this._cache[service];
					console.log('Loaded from dependecy cache [ ' + service + ' ]');
				}
				
				dependencies.push(serviceInstance);
			}
		}

		let instance;

		if(obj != null){
			instance = obj.bind.apply(obj,dependencies);
			return new instance();
		} else {
			console.error(className + ' LOAD FAILED!');
			throw className + 'LOAD FAILED';
		}
		

		
	}
};

module.exports = Loader;