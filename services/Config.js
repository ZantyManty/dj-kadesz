class Config {
	constructor(){
		var path = require('path');
		this._appDir = path.dirname(require.main.filename);
		this._config = require(this._appDir + '/config/config.json');
	}
	
	getAppDir(){
		return this._appDir;
	}
	
	getToken(){
		return this._config.token;
	}
	
	getPrefix(){
		return this._config.prefix;
	}
	
	getCommands(){
		return this._config.commands;
	}
	
	getEmoji(name){
		return this._config.emojis[0][name];
	}
	
	getDefaultVolume(){
		return this._config.defaultVolume;
	}
	
	getTimeZone(){
		return this._config.timeZone;
	}

	getMusicFolder(){
		return this._config.musicFolder;
	}
};

module.exports = Config;