class DiscordHandler {
	
	constructor(Config,DiscordUtility,Command,Music,Talk_Talk){
		
		this._DiscordUtility = DiscordUtility;
		this._Config = Config;
		this._Music = Music;
		this._Talk_Talk = Talk_Talk;
		this._Command = Command;
		
		this._Client = null;
	}
	
	handleCommand(commandData,message){
		/*
		* Ha létezik a parancs
		*/
		if(commandData.commandExists == true){
			console.log('Command Found [ ' + commandData.command + ' ]');
			this._Command.executeCommand(commandData.command,message);
		} else {
			return false;
		}
	}
	
	/*
	* Ha üzenet jön
	*/
	onMessage(message){
		let messageData = this.getMessageMeta(message);

		
		this._DiscordUtility.getGuildDataIds(message);
		
		console.log(messageData.messageContent);
		
		this._Talk_Talk.setChannel(messageData.textChannel);
	
		//Kiabálás ellenőrzés
		this._Talk_Talk.checkYells(messageData.messageContent);
		
		this._Music.setChannels(messageData.voiceChannel,messageData.textChannel);
		
		/*
		* Parancsok ellenőrzése
		*/
		if(this._Command.searchCommand(messageData.messageContent,this._Client.user.id) == true){
			let commandData = this._Command.parseCommand(messageData.messageContent);
			if(this.handleCommand(commandData,message) == false){
				messageData.textChannel.send('Nem létező parancs,úgyhogy ezt beszoptad!');
			}
		}
		
	}
	
	//https://discordjs.guide/popular-topics/reactions.html#listening-for-reactions-on-old-messages
	async onMessageReactionAdd(reaction, user){

		if (reaction.partial){
			try {
				await reaction.fetch();
			} catch (error) {
				console.log('Something went wrong when fetching the message: ', error);
				return;
			}
		}
		
		/*
		* NOT MY IDEA!
		* Credits to: Malexion
		* https://github.com/Malexion/Rhythm-Bot/blob/master/src/bot/bot.ts#L225
		*/
		if(reaction.message.author.id == this._Client.user.id && user.id != this._Client.user.id){
			
			if(reaction.emoji.name == this._Config.getEmoji("repeatSong")){
				this.handleCommand({command: "repeatSong",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("repeatList")){
				this.handleCommand({command: "repeatList",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("shuffle")){
				this.handleCommand({command: "shuffle",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("skip")){
				this.handleCommand({command: "skip",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("previous")){
				this.handleCommand({command: "previous",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("play")){
				this.handleCommand({command: "resume",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("pause")){
				this.handleCommand({command: "pause",commandExists: true},reaction.message);
			} else if(reaction.emoji.name == this._Config.getEmoji("stop")){
				this.handleCommand({command: "stop",commandExists: true},reaction.message);
			} /*else if(reaction.emoji.name == this._Config.getEmoji("highVolume")){
				this._Music.volumeUp();
			} else if(reaction.emoji.name == this._Config.getEmoji("lowVolume")){
				this._Music.volumeDown();
			}*/
			
			reaction.users.remove(user.id);
		}
		
		//console.log(reaction._emoji);
	}
	
	ready(Client){
		this._Client = Client;
		console.log('Logged in as ' + this._Client.user.tag + '!');
		console.log('Id:' + this._Client.user.id);
	}
	
	/*
	* Meta adatok:
	* - Szerver Id (guild)
	* - Chat csatorna
	* - Call csatorna
	* - Üzenet tartalma
	*/
	getMessageMeta(message){
		let meta = {};

		meta.guildId = message.guild.id;
		meta.textChannel = message.channel;
		meta.voiceChannel = message.member.voice.channel;
		meta.messageContent = message.content;
		
		return meta;
	}
	
};

module.exports = DiscordHandler;