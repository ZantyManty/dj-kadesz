class Utility {
	
	constructor(Config){
		this._Config = Config;
	}
	
	writeToJson(file,data){
		fs.writeFileSync(this._Config.getAppDir() + file, JSON.stringify(data, null, 4));
	}
	
	//Source: https://flaviocopes.com/how-to-uppercase-first-letter-javascript/
	capitalize(s){
		if (typeof s !== 'string') return ''
		return s.charAt(0).toUpperCase() + s.slice(1);
	}
	
	getRandomInt(max){
		return Math.floor(Math.random() * max);
	}
	
	//Source: https://stackoverflow.com/a/6274381
	shuffle(a) {
		for (let i = a.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			[a[i], a[j]] = [a[j], a[i]];
		}
		return a;
	}
	
	convertDuration(intTime,type){
		/*
		* https://stackoverflow.com/a/25279399
		*/
		var date = new Date(0);
		date.setSeconds(intTime); // specify value for SECONDS here
		
		let start = 0;
		let length = 0;
		
		if(type == 'm'){
			start = 14;
			length = 5;
		} else {
			start = 11;
			length = 8;
		}
		
		return date.toISOString().substr(start, length);
	}
	
	getStartTimeByClock(songDurationInt){
		let date = new Date();
		date.setSeconds(songDurationInt); // specify value for SECONDS here
				
		let start = 10;
		let length = 5;
		let dateOptions = { timeZone: this._Config.getTimeZone(),hour12: false };

		return date.toLocaleString('en-US', dateOptions).substr(start, length);
	}
};

module.exports = Utility;