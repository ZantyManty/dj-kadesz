class MusicData {

    constructor(Config,YouTubeInfo,YouTubeDownloader,NodeID3){
        this._Config = Config;

        /*
        * Mp3
        */
        this._nodeId3 = NodeID3;

		/*
		* YouTube csomagok
		*/
		this._ytInfo = YouTubeInfo;
        this._ytDownloader = YouTubeDownloader;

        this._process = require('process');
    }

    setRequestBy(username){
        this._username = username;
        return this;
    }

    async getSongObject(url){
        //Megnézzük,hogy fájlnév-e
        let re = new RegExp('\.(?:mp3|wav)$');
        let song = {};

        if(re.test(url.trim())){
            //Mp3 vagy Wav
            song = this.File(url);
        } else {
            //YouTube
            song = await this.YouTube(url);
        }

        song.requestedBy = this._username;

        return song;
    }

    File(fileName){
        let song = {};

        let fullPath = this._process.cwd() + '/' + this._Config.getMusicFolder() + '/' + fileName;

        let tags = this._nodeId3.read(fullPath);

        let artist = (tags.artist != undefined) ? tags.artist + ' - ' : '';

        song.title = artist + (tags.title != undefined ? tags.title : fileName);
        song.duration = 0;
        song.url = '"' + fullPath + '"';

        return song;
    }

    async YouTube(url){
        
        var obj = this;
        var song = {};

        await this._ytInfo.getInfo(url)
        .then(info => {
            console.log(info.items[0]);

            //Ha létezik a video
            if(info.items[0].webpage_url !== 'undefined'){
                
                song.title = info.items[0].title.replace(/(?:\(|\[)\s*(?:Official|Audio)[\w\s]*?[\)\]]/i,'');
                song.duration = info.items[0].duration;
                song.url = obj._ytDownloader(info.items[0].webpage_url,{filter: "audioonly"});
            } else {
                song.error = 'Zene nem található!';
            }
            
        })
        .catch((error) => {
            song.error = ':x: Szám hiba! (Nem található?)';
        });

        return song;
    }

}
module.exports = MusicData;