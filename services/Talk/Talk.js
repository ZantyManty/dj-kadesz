class Talk_Talk {
    
	constructor(Config,DiscordUtility,Utility,Emoji,Talk_Answer,Talk_Question){
		this._Config = Config;
		this._DiscordUtility = DiscordUtility;
		this._Utility = Utility;
		this._Emoji = Emoji;
		
		this._Question = Talk_Question;
		this._Answer = Talk_Answer;
		
		this._dictionary = require(this._Config.getAppDir() + '/models/dictionaries/dictionary.json');
		this._textChannel = null;
		
		this._yellCounter = 0;
	}
	
	setChannel(textChannel){
		this._textChannel = textChannel;
	}
	
	/*
	* Kiabálás ellenőrzése
	*/
	checkYells(message){
		let regex = /[A-ZÖÜÓÚŐŰÁÉÍ]{6,}/
		if(regex.test(message)){
			this._yellCounter++;
		}
		
		if(this._yellCounter == 6){
			this._yellCounter = 0;
			
			let length = this._dictionary.yellRespond.length;
			let random = this._Utility.getRandomInt(length);
			this._textChannel.send(this._dictionary.yellRespond[random]);
		}
	}
	
	/*
	* Üzenet kezelése
	*/
	talkHandle(message){
		let messageContent = message.content;
		let isQuestion = /\?$/g.test(messageContent);
		
		/*
		* Ha kérdés
		*/
		if(isQuestion){
			//Válasz visszaküldése
			this._textChannel.send(this._Question.recogniseQuestions(messageContent));
		} else {
			/*
			* Sértés
			*/
			let personalPronounRegex = this._dictionary.personalPronoun.join('|');
			let offendRegex = this._dictionary.offends.join('|');
			let regex = new RegExp('\.?(' + personalPronounRegex + ')?\\s?(' + offendRegex + ')\\s?(vagy!*)?', "i");
			let match = messageContent.match(regex);
			
			if(match != null){
				/*
				* Mérges reakció
				*/
				let random = this._Utility.getRandomInt(this._dictionary.angryReplyEmoji.length - 1);
				let emoji = this._dictionary.angryReplyEmoji[random].replace(/:/g,'');
				message.react(this._Emoji.get(emoji));
				
				/*
				* Válasz visszaküldése
				*/
				this._textChannel.send(this._Answer.recogniseOffends(match));
			} 
			/*
			* Dícséret
			*/
			else {
				let complimentRegex = this._dictionary.compliments.join('|');
				regex = new RegExp('\.?(' + complimentRegex + ')', "i");
				match = messageContent.match(regex);
				if(match != null){
					//Válasz visszaküldése
					this._textChannel.send(this._Answer.recogniseCompliment(match));
				}
			}
		}
	}
};

module.exports = Talk_Talk;