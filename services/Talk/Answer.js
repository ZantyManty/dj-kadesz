class Talk_Answer {
    
	constructor(Config,Utility){
		this._Config = Config;
		this._Utility = Utility;
		
		this._messageCache = new Map();
		this._dictionary = require(this._Config.getAppDir() + '/models/dictionaries/dictionary.json');
	}
	
	/*
	* Dícséretek kezelése
	*/
	recogniseCompliment(match){
		var firstElement = match[1].toString().toLowerCase().trim();
		
		//Megköszönés
		if(firstElement != undefined && firstElement.startsWith('köszön')){
			
			let weolcomeLength = this._dictionary.yourWelcomeReply.length;
			let random = this._Utility.getRandomInt(weolcomeLength);
			let yourWeolcomeText = this._dictionary.yourWelcomeReply[random];
			return yourWeolcomeText + '!';
			
		} else { //Dícséret fogadás
		
			let thanksLength = this._dictionary.thanksReply.length;
			let random = this._Utility.getRandomInt(thanksLength);
			let thanksText = this._dictionary.thanksReply[random];
			return thanksText + '!';
		}
		
	}
	
	/*
	* Sértések kezelése
	*/
	recogniseOffends(match){
		/*
		* Ha felismerte/vannak sértések
		*/
		var personalPronounStrong = (match[1] != undefined) ? match[1].toString() : '';
		
		/*
		* Ha a bot kapja a sértést SZEMÉLYES NÉVMÁSSAL
		*/
		if(typeof match[1] != undefined && personalPronounStrong.toLowerCase().indexOf("te") != -1){
			
			let random = this._Utility.getRandomInt(50);
			
			if((random % 3) == 0){
				random = this._Utility.getRandomInt(20);
				//Fix mondatos visszaoltás tárból
				if((random % 2) == 0){
					
					let swearsLength = this._dictionary.confrontReplyFix.length;
					random = this._Utility.getRandomInt(swearsLength);
					let confrontSwear = this._dictionary.confrontReplyFix[random];
					return confrontSwear + '!';
					
				} else { //Te meg egy... oltás
					let swearsLength = this._dictionary.confrontReply.length;
					random = this._Utility.getRandomInt(swearsLength);
					let confrontSwear = this._dictionary.confrontReply[random];
					return 'Te meg egy ' + confrontSwear + '!';
				}
				
			} else {
				//Random káromkodás a végére
				let swearsLength = this._dictionary.swears.length;
				let random = this._Utility.getRandomInt(swearsLength);
				let swear = this._dictionary.swears[random];
				
				//Eltároljuk a legfrissebb sértést
				this._messageCache['previousSwear'] = match[2];

				return this._Utility.capitalize(match[2]) + ' vagyok ' + swear + '?';
			}
			
		} 
		/*
		* Ha másvalakire mondjuk a sértést
		*/
		else if(typeof match[1] != undefined && match[1] == 'Ő'){
			return 'Ki a ' + match[2] + '?';
			
		} 
		/*
		* Ha nincs személyes névmás
		*/
		else {
			
			//Random válasz káromkodás
			let arrayLegth = this._dictionary.swearReply.length;
			let random = this._Utility.getRandomInt(arrayLegth);
			let swearReply = this._dictionary.swearReply[random];
			
			//Random mérges emoji
			arrayLegth = this._dictionary.angryReplyEmoji.length;
			random = this._Utility.getRandomInt(arrayLegth);
			let emojiReply = this._dictionary.angryReplyEmoji[random];
			
			if(personalPronounStrong.toLowerCase().indexOf("igen") != -1 || match[2] != undefined){
				/* 
				* Ha "az" van benne,akkor visszautalunk egy előző sértésre,
				* amit a this._messageCache 'previousSwear' kulcsa alatt tárolunk
				*/
				if(match[2] != undefined && match[2] == 'az'){
					if(this._messageCache['previousSwear'] != undefined){
						return 'Szóval ' + this._messageCache['previousSwear'] + ' vagyok... \nAkkor ' + swearReply + '! ' + emojiReply;
					} else {
						return 'Na mi vagyok?';
					}
				} 
				/*
				* Ha csak sima sértés
				*/
				else if(match[2] != undefined){
					//Eltároljuk a legfrissebb sértést
					this._messageCache['previousSwear'] = match[2];
					
					return 'Azt mondtad,hogy egy ' + match[2]  +' vagyok... Akkor ' + swearReply + '! ' + emojiReply;
				} 
				/*
				* Ha nincs sértés,csak sima "Igen" van
				*/
				else {
					return 'Mi igen?';
				}
			}
		}
	}
	
	
};

module.exports = Talk_Answer;