class Talk_Question {
    
	constructor(Config,Utility,Memory){
		this._Config = Config;
		this._Memory = Memory;
		this._Utility = Utility;
		
		this._dictionary = require(this._Config.getAppDir() + '/models/dictionaries/dictionary.json');
	}
	
	/*
	* Kérdések felismerése
	*/
	recogniseQuestions(message){
		/*
		* Group 1: Kérdőszó
		* Group 2: Kifejezés
		* Group 3: rag (Tárgyas)
		* Group 4: rag (Birtokos)
		*
		* https://regexr.com/591jp
		*/
		let match = message.match(/^\s*([\w]+)(?:\s*[a-záé]{0,5}\s){0,1}([\s\S]+?)(?:-*((?:e|a|é|á)*t)|((?:o|e)?(?:d|m)(?:a|e)?t?))\?\s*/i);
		console.log(match);
		console.log(message);
		
		if(match != null){
			let topic = match[2].toString().toLowerCase().trim();
			let firstWord = match[1].toString().toLowerCase().trim();
			
			if(firstWord != undefined && firstWord == 'szereted'){
				/*
				* Ha van tárolt válasz
				*/
				if(this._Memory.checkQuestionAnswer(topic)){
					let answer = this._Memory.getQuestionAnswer(topic);
					return 'Mondtam már,hogy ' + answer;
				} 
				/*
				* Ha nincs,akkor generálunk
				*/
				else {
					let replyType = (this._Utility.getRandomInt(50) % 2 == 0) ? 'replyYes' : 'replyNo';
					let replyArrLength = this._dictionary[replyType].length;
					let random = this._Utility.getRandomInt(replyArrLength);
					let answer = this._dictionary[replyType][random];
					
					this._Memory.addQuestionAnswer(topic,answer);
					return answer;
				}
			}
			
			/*
			* Beleírjuk a memória fájlba az új válaszokat
			*/
			this._Memory.exportMemory();
			
			return 'Faszom se tudja...';
		} else {
			return 'Mi van?...';
		}
	}
};

module.exports = Talk_Question;