class Memory {
	
	constructor(Config,Utility){
		this._Config = Config;
		this._Utility = Utility;
		this.loadMemory();
	}
	
	loadMemory(){
		let path = this._Config.getAppDir() + "/storage/memory.json";
		
		if (fs.existsSync(path)) {
			/*
			* Memória betöltése
			*/
			this._memory = require(path);
		} else {
			/*
			* Memória objektum felépítése
			*/
			this._memory = {};
			this._memory.question = {};
		}
	}
	
	exportMemory(){
		this._Utility.writeToJson("/storage/memory.json",this._memory);
	}
	
	checkQuestionAnswer(topic){
		return !(this._memory.question[topic] === undefined);
	}
	
	getQuestionAnswer(topic){
		return this._memory.question[topic];
	}
	
	addQuestionAnswer(topic,answer){
		this._memory.question[topic] = answer;
	}
};

module.exports = Memory;