class Bot {
	
	constructor(Discord,DiscordHandler,Config){
		this._Config = Config;
		
		this._Discord = Discord;
		this._DiscordHandler = DiscordHandler;
	}
	
	/*
	* Listen metódus
	*/
	async listen(){
		console.log('Listen...');
		
		let Client = new this._Discord.Client();
		var _DiscordHandler = this._DiscordHandler;
		
		/*
		* Event-ek
		*/
		Client.on('message',function(message){ _DiscordHandler.onMessage(message); });
		Client.on('messageReactionAdd',function(reaction, user){ _DiscordHandler.onMessageReactionAdd(reaction, user); });
		Client.once('ready',function(){ _DiscordHandler.ready(Client); });
		
		Client.once('reconnecting', () => {
			console.log('Reconnecting!');
		});

		Client.once('disconnect', () => {
			console.log('Disconnect!');
		});
		
		
		Client.login(this._Config.getToken());
	}
	
};

module.exports = Bot;