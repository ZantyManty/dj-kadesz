class DiscordUtility {
	
	constructor(Discord,Config,Utility){
		this._Discord = Discord;
		this._Config = Config;
		this._Utility = Utility;
		this.loadData();
	}
	
	loadData(){
		let path = this._Config.getAppDir() + "/storage/";
		this._DiscordData = (fs.existsSync(path)) ? require(path + "discordData.json") : {};
		
		if(Object.keys(this._DiscordData).length > 0){
			console.log('Discord ID data FOUND!');
		}
	}
	
	exportData(){
		this._Utility.writeToJson('/storage/discordData.json',this._DiscordData);
	}
	
	getGuildDataIds(message){
		
		if(Object.keys(this._DiscordData).length > 0) return;
		
		/*
		* Members
		*/
		var memberIds = {};
		let guildMembers = message.member.guild.members.cache;
		
		this.fetchMapData(guildMembers,function (item){
			memberIds[item.user.id] = item.user.username;
		});
		
		this._DiscordData.members = memberIds;
		
		/*
		* Channels
		*/
		var channelIds = {};
		let guildChannels = message.guild.channels.cache;
		
		this.fetchMapData(guildChannels,function (item){
			channelIds[item.name] = item.id;
		});
		
		this._DiscordData.channels = channelIds;
		
		/*
		* Emojik
		*/
		var emojiIds = {};
		let guildEmojis = message.guild.emojis.cache;
		
		this.fetchMapData(guildEmojis,function (item){
			emojiIds[item.name] = item.id;
		});
		
		this._DiscordData.emojis = emojiIds;
		
		this.exportData();
	}
	
	fetchMapData(map,callBack){
		var arr = [];
		map.forEach(callBack);
		return arr;
	}
	
	createEmbed(title,color,author){
		let Embed = new this._Discord.MessageEmbed();
		
		Embed.setTitle(title);
		
		if(color != undefined){
			Embed.setColor(color);
		}
		
		if(author != undefined){
			Embed.setAuthor(author);
		}
		
		return Embed;
		
	}
	
	/*
	* GETs
	*/
	getGuildMemberNameById(id){
		return this._DiscordData.members[id];
	}
	
	getGuildChannelByName(name){
		return this._DiscordData.channels[name];
	}
	
	getGuildEmojiByName(name){
		return this._DiscordData.emojis[name];
	}
	
};

module.exports = DiscordUtility;