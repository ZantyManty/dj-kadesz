class Command {
	
	constructor(Config,Loader){
		this._Loader = Loader;
		this._Config = Config;
	}
	
	searchCommand(messageContent,userId){
		let mentionRegex = new RegExp('<@!*' +userId + '>','i');
	
		//Ha SE prefix SE mention nincs,akkor hagyja
		return !(mentionRegex.test(messageContent) == false && messageContent.startsWith(this._Config.getPrefix()) == false);
	}
	
	parseCommand(messageContent){
		/*
		* Parancsok értelmezése
		*/
		let regex = /^!([\w]+)/i;
		let commandMatch = messageContent.match(regex);
		let command = (commandMatch != null) ? commandMatch[1] : '';
		
		//Üzenet kitisztítása
		messageContent = this.cleanMessage(messageContent);
		
		//Ha létezik a parancs
		if(this._Config.getCommands().includes(command)){
			return {commandExists: true,command: command,messageContent: messageContent};
		} else {
			return {commandExists: false,messageContent: messageContent};
		}
	}
	
	cleanMessage(messageContent){
		return messageContent.replace(/^(?:![\w]+)|<@!*[\w]+?>/i,'').trim();
	}
	
	async executeCommand(command,message){
		let commandName = 'Command_' + command;
		let commandClass = this._Loader.resolve(commandName);
		message.content = this.cleanMessage(message.content);
		await commandClass.execute(message);
		message.delete({ timeout: 6500 });
	}
	
}

module.exports = Command;