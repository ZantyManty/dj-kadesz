class Music {
    
	constructor(Config,DiscordUtility,Utility){
		this._voiceChannel = null;
		this._textChannel = null;
		this._connection = null;
		this._dispatcher = null;
		
		this._Config = Config;
		this._Utility = Utility;
		this._DiscordUtility = DiscordUtility;
	
		this._songs = [];
		this._currentMusicIndex = 0;
		
		this._isPlaying = false;
		this._isRepeatList = false;
		this._currentRepeatSong = -1;
		this._volume = this._Config.getDefaultVolume();
	}
	
	setChannels(voiceChannel,textChannel){
		this._voiceChannel = voiceChannel;
		this._textChannel = textChannel;
	}
	
	setYtdl(ytdl){
		this._ytdl = ytdl;
	}
	
	async joinVoiceChannel(){
		//Ha nem call-ban vagy
		if (!this._voiceChannel){
			this._textChannel.send(
			  "You need to be in a voice channel to play music!"
			);
			return;
		}

		this._connection = await this._voiceChannel.join();
		
		return true;
	}
	
	addSong(song){
		this._songs.push(song);
	}
	
	async play(){
		let song = this._songs[this._currentMusicIndex];
		
		if(typeof(song) == 'undefined') return;
		
		this._isPlaying = true;
		this._textChannel.send(song.title);
		
		console.log(song);

		/*
		* Megvárjuk,míg kapcsolódik
		*/
		await this.joinVoiceChannel();

		/*
		* Maga a lejátszás
		*/
		this._dispatcher = this._connection
		.play(song.url)
		.on("finish", () => {
			this._isPlaying = false;
			/*
			* Ha teljes lejátszási lista végtelenítve van
			*/
			if(this._isRepeatList == true && (this._songs.length - 1) == this._currentMusicIndex){
				this._currentMusicIndex = 0;
			} 
			/*
			* Egy darab szám végtelenített ismétlése
			*/
			else if(this._currentRepeatSong > -1 && this._currentRepeatSong == this._currentMusicIndex) {
				this._currentMusicIndex = this._currentRepeatSong;
			} 
			/*
			* Sima lejátszásnál továbbhaladunk
			*/
			else {
				this._currentMusicIndex++;
			}
			
			this.play();
		})
		.on("error", error => console.error(error));
		
	}
	
	skip(){
		this._dispatcher.end();
	}
	
	repeatList(){
		if(this._isRepeatList == false)
			this._isRepeatList = true;
		else
			this._isRepeatList = false;
	}
	
	repeatSong(){
		if(this._currentRepeatSong == -1)
			this._currentRepeatSong = this._currentMusicIndex;
		else
			this._currentRepeatSong = -1;
	}
	
	stop(){
		this._songs = [];
		this._currentMusicIndex = 0;
		this._currentRepeatSong = -1;
		this._dispatcher.end();
		this._isPlaying = false;
	}
	
	resume(){
		if(this._isPlaying == false){
			this._dispatcher.resume();
			this._isPlaying = true;
		}
	}
	
	pause(){
		if(this._isPlaying == true)
		{ 
			this._dispatcher.pause(); 
			this._isPlaying = false;
		}
	}
	
	volumeUp(){
		this._volume += 0.25;
		this._dispatcher.setVolume(this._volume);
		
	}
	
	volumeDown(){
		this._volume -= 0.25;
		this._dispatcher.setVolume(this._volume);
	}
	
	previous(){
		this._currentMusicIndex -= 2;
		if(this._isPlaying == true)
			this.skip();
	}
	
	shuffle(){
		let songsLocal = this._songs;
		this.stop();
		this._songs = this._Utility.shuffle(songsLocal);
		this._currentMusicIndex -= 1;
		songsLocal = null;
		this.play();
	}
	
	remove(index){
		if((index - 1) == this._currentMusicIndex)
			this.skip();
		
		this._songs[index - 1] = undefined;
		this._songs = this._songs.filter(val => val);
	}
	
	async playlist(){
		
		if (this._songs !== 'undefined' && this._songs.length > 0) {
			let Embed = this._DiscordUtility.createEmbed("Mai ajánlatom","#eb1414");
			
			let fullDurration = 0;
			let songTimes = 0;
			for(let k in this._songs){
				if(this._songs[k] != undefined){
					let number = parseInt(k) + 1;
					let titleData = this._songs[k]['title'].split("-");
					let author = (titleData[0] != undefined) ? titleData[0] : 'Ismeretlen';
					let title = (titleData[1] != undefined) ? titleData[1] : titleData[0];
					
					/*
					* Ismétlés állapot kijelzése
					*/
					let isCurrent = (k == this._currentMusicIndex) ? ':musical_note: ' : '';
					let isRepeated = (k == this._currentRepeatSong) ? ' :repeat_one: ' : '';
					
					/*
					* Összes hossz
					*/
					fullDurration += this._songs[k]['duration'];
					
					/*
					* Óra szerinti kezdés kiszámítása
					*/
					let whenComes = '';
					
					if(k > this._currentMusicIndex){
						whenComes = ' ~:clock1: ' + this._Utility.getStartTimeByClock(songTimes + 20);
					}
					
					if(k >= this._currentMusicIndex){
						songTimes += (this._songs[k]['duration'] + 20);
					}
					
					/*
					* Zene kiírás hozzáadása
					*/
					let infoField = isCurrent + 
					isRepeated + 
					number +') ' + 
					title;
					
					/*
					* Idő adatok (HA vannak)
					*/
					if(this._songs[k]['duration'] != undefined){
						infoField += 
						' (' + this._Utility.convertDuration(this._songs[k]['duration'],'m') + ') ' + 
						whenComes;
					}

					infoField += '[' + this._songs[k]['requestedBy'] + ']';

					Embed.addField(
						infoField,
					author);
				}
			}
			
			let isRepeatedList = (this._isRepeatList == true) ? ' ( :repeat: )' : '';

			//Ha van bármilyen hossz adat
			if(songTimes != 0 && fullDurration != 0){
				let whenEnds = ' ~:clock1: ' + this._Utility.getStartTimeByClock(songTimes);
				Embed.setDescription('Lejátszási idő: ' + this._Utility.convertDuration(fullDurration,'h') + whenEnds + isRepeatedList);
			}
			
			/*
			* Reakció gombok kirakása
			* Inspired by Rhytm-bot
			* Credits to: Malexion
			* https://github.com/Malexion/Rhythm-Bot/blob/master/src/media/media-player.ts#L108
			*/
			var obj = this;
			await this._textChannel.send(Embed).then((message) => {
				message.react(this._Config.getEmoji("play"));
				message.react(this._Config.getEmoji("pause"));
				message.react(this._Config.getEmoji("stop"));
				message.react(this._Config.getEmoji("previous"));
				message.react(this._Config.getEmoji("skip"));
				message.react(this._Config.getEmoji("repeatList"));
				message.react(this._Config.getEmoji("repeatSong"));
				message.react(this._Config.getEmoji("shuffle"));
			});
		} else {
			this._textChannel.send("Nincsenek zenék...");
		}
	}
	
	isPlaying(){ return this._isPlaying; }
	setPlaying(bool){ this._isPlaying = bool; }
	
};

module.exports = Music;