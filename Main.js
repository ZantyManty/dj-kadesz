class Main {
	
	constructor(fs,classDirecotiries){
		this._fs = fs;
		this._classDirecotiries = classDirecotiries;
		
		this._Loader = require('./loaders/loader.js');
		this.setUpLoader();
	}
	
	setUpLoader(){
		/*
		* Megfelelő modulok betöltése
		*/
		this._Loader = new this._Loader(fs);
		this._Loader.loadServices('./config/services.json');
	}
	
	async start() {
		/*
		* Bot betöltése
		*/
		var Bot = await this._Loader.collectConstructorData(this._classDirecotiries).then(
			(data)	=> { return this._Loader.resolve('Bot'); },
			(err)	=> { console.error(err); }
		);
		
		/*
		* Bot indítása
		*/
		Bot.listen();
	}
	
};

module.exports = Main;