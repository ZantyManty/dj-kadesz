class Command_skip {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.skip();
	}
};

module.exports = Command_skip;