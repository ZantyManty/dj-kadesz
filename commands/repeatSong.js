class Command_repeatSong {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.repeatSong();
	}
};

module.exports = Command_repeatSong;