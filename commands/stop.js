class Command_stop {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.stop();
	}
};

module.exports = Command_stop;