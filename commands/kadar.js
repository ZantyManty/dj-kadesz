class Command_kadar {
	
	constructor(Talk_Talk){
		this._Talk_Talk = Talk_Talk;
	}
	
	execute(message){
		this._Talk_Talk.talkHandle(message);
	}
};

module.exports = Command_kadar;