class Command_resume {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.resume();
	}
};

module.exports = Command_resume;