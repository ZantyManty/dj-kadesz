class Command_repeatList {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.repeatList();
	}
};

module.exports = Command_repeatList;