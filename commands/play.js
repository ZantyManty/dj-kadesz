class Command_play {
	
	constructor(Music,MusicData,Utility){
		this._Music = Music;
		this._MusicData = MusicData;
		this._Utility = Utility;
		
	}
	
	async execute(message){
		let args = message.content.split("\n");
		var obj = this;
		
		if(this._Music.joinVoiceChannel()){

			//Zene promise-ok
			var promises = args.map(function(name) {
			  return new Promise(function(resolve, reject) {

				  message.channel.send(':mag: ' + name).then((message) => { message.delete({ timeout: 5000 }); });

				  var song = {};

				  (async () => {
					song = await obj._MusicData.setRequestBy(message.author.username).getSongObject(name);
					
					/*
					* Hibakezelés
					*/
					if(typeof song.error !== "undefined"){
						reject(song);
					} else {
						resolve(song);
					}
					
				  })().then(song => {
					resolve(song);
				  })
				  .catch(song => {
					message.channel.send(song.error);
					reject(song);
				  });

			  })
			  .then(song => { 
				  obj._Music.addSong(song);
				  message.channel.send(':white_check_mark: ' + song.title).then((message) => { message.delete({ timeout: 6000 }); });
				})
			  .catch(song => { 
				message.channel.send(song.error);
			   });
			});
			
			/*
			 * Zene promise-ok kezelése 
			 */
			Promise.all(promises)
			.then(function() { 
				try {
					//Zene lejátszás
					if(obj._Music.isPlaying() == false)
						obj._Music.play();

				} catch (err) {
				  return message.channel.send(err);
				}
			})
			.catch(function(err){
				console.log(err);
			});
		}
	}
}

module.exports = Command_play;