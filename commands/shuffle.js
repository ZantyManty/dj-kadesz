class Command_shuffle {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.shuffle();
	}
};

module.exports = Command_shuffle;