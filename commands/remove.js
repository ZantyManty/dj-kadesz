class Command_remove {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		let match = message.content.match(/([\d+])/i);
		
		if(match != null){
			this._Music.remove(match[1]);
		} else {
			message.channel.send('Há\' ez nem szám tess');
		}
	}
};

module.exports = Command_remove;