class Command_previous {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.previous();
	}
};

module.exports = Command_previous;