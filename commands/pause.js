class Command_pause {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.pause();
	}
};

module.exports = Command_pause;