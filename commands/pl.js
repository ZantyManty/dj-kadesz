class Command_pl {
	
	constructor(Music){
		this._Music = Music;
		
	}
	
	execute(message){
		this._Music.playlist();
	}
};

module.exports = Command_pl;